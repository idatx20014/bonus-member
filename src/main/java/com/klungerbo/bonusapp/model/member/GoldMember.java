package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;
import java.time.LocalDate;

/**
 * Representation for gold membership.
 *
 * @author Tomas Klungerbo Olsen
 * @since 0.2
 */
public class GoldMember extends BonusMember {
  public static final float FACTOR_GOLD = 1.5f;

  public GoldMember(final int memberNo,
                    final Personals personals,
                    final LocalDate localDate,
                    final int points) {
    super(memberNo, personals, localDate);
    this.addPoints(points);
  }

  /**
   * Apply bonus points and register it for the bonus member.
   *
   * @param points the points to add
   */
  @Override
  public void registerPoints(int points) {
    points *= FACTOR_GOLD;
    this.addPoints(points);
  }
}
