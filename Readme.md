[![Documentation: JavaDoc](https://img.shields.io/badge/Documentation-JavaDoc-blue)](http://idatx20014.pages.stud.idi.ntnu.no/bonus-member/com.klungerbo.bonusapp/module-summary.html)
[![pipeline status](https://gitlab.stud.idi.ntnu.no/idatx20014/bonus-member/badges/master/pipeline.svg)](https://gitlab.stud.idi.ntnu.no/idatx20014/bonus-member/commits/master)
# Airline bonus member manager 
This Application keeps track of and manages bonus members for an airline company. It has the functionality to register new members as well as deleting and upgrading them. 

![](readme-resources/pie-chart.gif)

## Requirements
To compile this project a [Java version 13](https://jdk.java.net/archive/) with previews enabled is required.

### Setting project SDK in IntelliJ to Java 13
To set the SDK to version 13 for this project go to [File] -> [Project Structure...] Look for the
 Project SDK section under Project Settings, Project. Click NEW then (+) JDK and select the
  installed JDK-13 directory. Also, make sure that the Project language level is set to **13
   (Preview) - Switch expressions, text blocks**. 
   
![](readme-resources/project-sdk.png)

Then under Project Settings, Modules make sure that the Language level is set to **13 (Preview
) - Switch expressions, text blocks**.

![](readme-resources/modules-language-level.png)

## Acknowledgments
* Icons used in the application is made by [icon king](https://freeicons.io/profile/3) from 
www.freeicons.io
* Icon used as project avatar is made by [Fasil](https://freeicons.io/profile/722) from 
www.freeicons.io
