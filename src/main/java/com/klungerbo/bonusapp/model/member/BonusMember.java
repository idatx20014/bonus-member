package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 * Data class for a bonus member.
 *
 * @author Tomas Klungerbo Olsen
 * @since 0.2
 */
public abstract class BonusMember implements Comparable<BonusMember> {
  private final int memberNo;
  private final Personals personals;
  private final LocalDate enrolledDate;
  private int bonusPoints;

  /**
   * Constructor saving passed values to fields.
   *
   * @param memberNo     the member number
   * @param personals    the personal information
   * @param enrolledDate the date of enrollment
   * @throws IllegalArgumentException if one or more arguments are invalid
   */
  protected BonusMember(final int memberNo,
                        final Personals personals,
                        final LocalDate enrolledDate) {
    if (memberNo < 0 || personals == null || enrolledDate == null) {
      throw new IllegalArgumentException("One or more arguments are invalid");
    }

    this.bonusPoints = 0;
    this.memberNo = memberNo;
    this.personals = personals;
    this.enrolledDate = enrolledDate;
  }

  /**
   * Get the number associated with this member.
   *
   * @return the member number
   */
  public int getMemberNo() {
    return this.memberNo;
  }

  /**
   * Get the personal information associated with this member.
   *
   * @return the personal information.
   */
  public Personals getPersonals() {
    return this.personals;
  }

  /**
   * Get the date this member enrolled as a member.
   *
   * @return the enrolled date
   */
  public LocalDate getEnrolledDate() {
    return this.enrolledDate;
  }

  /**
   * Finds the qualification points i.e. points that are register
   * from enrolled date up to maximum a year. If a year has passed
   * since enrollment there will be no qualification points.
   *
   * @param currentDate the current date
   * @return points if less than a year of membership, 0 if else.
   * @throws IllegalArgumentException if the date is not valid
   */
  public int findQualificationPoints(final LocalDate currentDate) {
    if (currentDate == null) {
      throw new IllegalArgumentException("Needs a valid date");
    }

    int returnValue = 0;

    int yearsBetween = Period.between(this.enrolledDate, currentDate).getYears();
    if (yearsBetween < 1) {
      returnValue = this.getPoints();
    }

    return returnValue;
  }

  /**
   * Get the points associated with this member.
   *
   * @return amount of points this member has
   */
  public int getPoints() {
    return this.bonusPoints;
  }

  /**
   * Delegates password checking to the personals.
   *
   * @param password the password to check
   * @return true if password is correct, false if else
   */
  public boolean okPassword(final String password) {
    boolean retVal = false;

    if (password != null && !password.isBlank()) {
      retVal = this.personals.okPassword(password);
    }

    return retVal;
  }

  /**
   * Register points to the member.
   *
   * @param points the points to add
   * @throws IllegalArgumentException if points are not valid
   */
  protected void addPoints(final int points) {
    if (points <= 0) {
      throw new IllegalArgumentException("Unable to add 0 or less points");
    }

    this.bonusPoints += points;
  }

  /**
   * Add points to this member.
   *
   * @param points amount of points to give the member
   */
  public abstract void registerPoints(final int points);

  /**
   * Creates a unique hash for this object.
   *
   * @return a unique hash connected with this object
   */
  @Override
  public int hashCode() {
    return Objects.hash(memberNo, personals, enrolledDate, bonusPoints);
  }

  /**
   * Deep compares if this object is equal to another object.
   *
   * @param o another object to compare against
   * @return true if they are equal, false if else
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BonusMember that = (BonusMember) o;
    return memberNo == that.memberNo
        && bonusPoints == that.bonusPoints
        && Objects.equals(personals, that.personals)
        && Objects.equals(enrolledDate, that.enrolledDate);
  }

  /**
   * Compare this members points with another members points.
   *
   * @param bonusMember the other member
   * @return 0 if they are equal, -1 if this is less and 1 if this is greater
   */
  @Override
  public int compareTo(final BonusMember bonusMember) {
    return Integer.compare(this.bonusPoints, bonusMember.bonusPoints);
  }
}
