package com.klungerbo.bonusapp.ui;

import com.klungerbo.bonusapp.model.Personals;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * Dialogue window responsible for taking accepted input
 * from the user of this application to create valid
 * bonus members.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public class AddBonusMemberDialog extends Dialog<Personals> {
  private Logger logger;

  // Fields to hold user input.
  private TextField firstName;
  private TextField surname;
  private TextField email;
  private PasswordField password;

  /**
   * Constructor used for default initialization.
   */
  public AddBonusMemberDialog() {
    super();

    this.logger = Logger.getLogger(getClass().toString());

    this.firstName = new TextField();
    this.surname = new TextField();
    this.email = new TextField();
    this.password = new PasswordField();

    this.createView();
  }

  /**
   * Create dialogue window.
   */
  private void createView() {
    setTitle("Add member");
    getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

    this.firstName.setPromptText("First name");
    this.surname.setPromptText("Surname");
    this.email.setPromptText("Email address");
    this.password.setPromptText("Password");

    this.createInputGrid();
    this.createOkEventConsumer();
    this.createCancelEventConsumer();
    this.createResultConverter();
  }

  /**
   * Creates the grid of label/input fields used for getting
   * user input.
   */
  private void createInputGrid() {
    final GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(20, 150, 10, 10));

    grid.add(new Label("First name:"), 0, 0);
    grid.add(firstName, 1, 0);
    grid.add(new Label("Surname:"), 0, 1);
    grid.add(surname, 1, 1);
    grid.add(new Label("Email address:"), 0, 2);
    grid.add(email, 1, 2);
    grid.add(new Label("Password:"), 0, 3);
    grid.add(password, 1, 3);

    this.getDialogPane().setContent(grid);
  }

  /**
   * Sets up an event consumer that stops the dialogue from
   * receiving this event in the resultConverter if input is invalid.
   * This is useful as it is not desired to accept invalid input.
   */
  private void createOkEventConsumer() {
    final Button buttonOk = (Button) this.getDialogPane().lookupButton(ButtonType.OK);
    buttonOk.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          if (
              firstName.getText().isBlank()
                  || surname.getText().isBlank()
                  || email.getText().isBlank()
                  || password.getText().isBlank()) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.getDialogPane()
                .getStylesheets()
                .add(this.getClass().getResource("/css/style.css").toExternalForm());
            alert.setTitle("Invalid input");
            alert.setHeaderText("All fields must be set");
            alert.setContentText("In order to add a new user all fields must be set");
            alert.showAndWait();

            event.consume();
          }
        });
  }

  /**
   * Sets up an event consumer that gives the user an option to not exit
   * the dialogue if there was a miss click.
   */
  private void createCancelEventConsumer() {
    final Button buttonCancel = (Button) this.getDialogPane().lookupButton(ButtonType.CANCEL);
    buttonCancel.addEventFilter(
        ActionEvent.ACTION,
        event -> {
          if (!firstName.getText().isBlank()
              || !surname.getText().isBlank()
              || !email.getText().isBlank()
              || !password.getText().isBlank()) {

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.getDialogPane()
                .getStylesheets()
                .add(this.getClass().getResource("/css/style.css").toExternalForm());
            alert.setTitle("Exit add member");
            alert.setHeaderText("Are you sure you want to Exit member creation?");
            alert.setContentText("If you press ok all the fields entered will be cleared");
            alert.showAndWait()
                .filter(response -> response == ButtonType.CANCEL)
                .ifPresent(response -> event.consume());
          }
        });
  }

  /**
   * Creates the return logic.
   */
  private void createResultConverter() {
    this.setResultConverter(
        (ButtonType button) -> {
          Personals result = null;
          if (button == ButtonType.OK) {
            try {
              result = new Personals(
                  firstName.getText(),
                  surname.getText(),
                  email.getText(),
                  password.getText()
              );
            } catch (IllegalArgumentException e) {
              this.logger.warning("Could not create a new Person as one or "
                  + "more arguments were invalid");
            }
          }
          return result;
        }
    );
  }
}
