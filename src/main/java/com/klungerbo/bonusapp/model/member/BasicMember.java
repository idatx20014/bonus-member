package com.klungerbo.bonusapp.model.member;

import com.klungerbo.bonusapp.model.Personals;
import java.time.LocalDate;

/**
 * Representation for basic membership.
 *
 * @author Tomas Klungerbo Olsen
 * @since 0.2
 */
public class BasicMember extends BonusMember {
  public BasicMember(final int memberNo,
                     final Personals personals,
                     final LocalDate localDate) {
    super(memberNo, personals, localDate);
  }

  /**
   * Add points to a basic member.
   *
   * @param points the points to add
   */
  @Override
  public void registerPoints(final int points) {
    this.addPoints(points);
  }
}
