package com.klungerbo.bonusapp.util;

/**
 * Implement this interface to make your class an observer.
 * Add any class that implements this interface as a subscriber to
 * a class that extends Observable to get notification on updates.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public interface Observer {
  /**
   * this method will be called when there is an update.
   */
  void update();
}
