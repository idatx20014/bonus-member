package com.klungerbo.bonusapp.util;

import java.util.ArrayList;

/**
 * An observable class that manages and notifies subscribers
 * on request.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public abstract class Observable {
  private ArrayList<Observer> observers = new ArrayList<>();

  /**
   * Subscribe as an observer to receive notifications on updates.
   *
   * @param observer the observer to subscribe
   */
  public void subscribe(final Observer observer) {
    this.observers.add(observer);
  }

  /**
   * Unsubscribe as an observer resulting in no more updates.
   *
   * @param observer the observer to unsubscribe
   */
  public void unSubscribe(final Observer observer) {
    this.observers.remove(observer);
  }

  /**
   * Notifies all subscribers to update.
   */
  protected void notifySubscribers() {
    this.observers.forEach(Observer::update);
  }
}
