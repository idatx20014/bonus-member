package com.klungerbo.bonusapp.application;

import com.klungerbo.bonusapp.ui.MemberCli;

/**
 * Application implementation for a command line interface.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public class CliApp implements BonusMemberApp {
  private MemberCli ui;

  public CliApp() {
    this.ui = new MemberCli();
  }

  @Override
  public void initialize(final String[] args) {
    this.ui.init(args);
  }

  @Override
  public void execute() {
    this.ui.execute();
  }
}
