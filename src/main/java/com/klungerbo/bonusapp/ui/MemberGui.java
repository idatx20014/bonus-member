package com.klungerbo.bonusapp.ui;

import com.klungerbo.bonusapp.application.BonusMemberApp;
import com.klungerbo.bonusapp.controller.MemberController;
import com.klungerbo.bonusapp.model.MemberArchive;
import com.klungerbo.bonusapp.model.Personals;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.model.member.GoldMember;
import com.klungerbo.bonusapp.model.member.SilverMember;
import com.klungerbo.bonusapp.util.Observer;
import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * Graphical user interface for the bonus member application using JavaFX.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public class MemberGui extends Application implements BonusMemberApp, Observer {
  private static final String ADD_USER_ICON = "/images/add_user.png";
  private static final String REMOVE_USER_ICON = "/images/remove_user.png";
  private static final String UPGRADE_USER_ICON = "/images/upgrade_users.png";
  private static final String CSS_DARK_THEME = "/css/style.css";
  private static final String BUTTON_LABEL_FONT_STYLE = "-fx-font-size:16";
  TableView<BonusMember> table;
  private MemberArchive model;
  private MemberController controller;
  private String[] args;
  private StringProperty goldPercentString;
  private StringProperty silverPercentString;
  private StringProperty basicPercentString;
  private StringProperty memberString;
  private IntegerProperty numberOfGold;
  private IntegerProperty numberOfSilver;
  private IntegerProperty numberOfBasic;
  private IntegerProperty numberOfMem;
  private ObservableList<PieChart.Data> pieChartData;
  private ObservableList<BonusMember> data;
  private ObjectProperty<BonusMember> selectedBonusMember;
  private VBox selectedMemberInfoView;
  private BorderPane root;

  private Random random;

  /**
   * Initialized the default state.
   */
  public MemberGui() {
    this.model = new MemberArchive();
    this.model.subscribe(this);
    this.controller = new MemberController(this.model);

    this.random = new Random();
    this.selectedBonusMember = new SimpleObjectProperty<>(null);
    this.selectedMemberInfoView = new VBox();
    this.root = new BorderPane();

    this.data = FXCollections.observableArrayList();
    this.pieChartData = FXCollections.observableArrayList();
    this.basicPercentString = new SimpleStringProperty("");
    this.goldPercentString = new SimpleStringProperty("");
    this.silverPercentString = new SimpleStringProperty("");
    this.memberString = new SimpleStringProperty("");
    this.numberOfBasic = new SimpleIntegerProperty(0);
    this.numberOfGold = new SimpleIntegerProperty(0);
    this.numberOfSilver = new SimpleIntegerProperty(0);
    this.numberOfMem = new SimpleIntegerProperty(0);
  }

  @Override
  public void initialize(String[] args) {
    this.args = args;
  }

  @Override
  public void execute() {
    launch(this.args);
  }

  @Override
  public void init() throws Exception {
    super.init();
    this.table = new TableView<>();
    this.generateTestData();
    this.refreshView();
  }

  @Override
  public void start(Stage stage) {
    createView();

    Scene scene = new Scene(root, 1280, 720);
    scene.getStylesheets().add(this.getClass().getResource(CSS_DARK_THEME).toExternalForm());
    stage.setScene(scene);
    stage.setTitle("Bonus member archive");
    stage.show();
  }

  @Override
  public void stop() throws Exception {
    super.stop();
    System.exit(0);
  }

  private void generateTestData() {
    final Lorem lorem = LoremIpsum.getInstance();
    final LocalDate currentDate = LocalDate.now();

    for (int i = 0; i < 256; i++) {
      final String firstName = lorem.getFirstName();
      final String surname = lorem.getLastName();
      final String email = lorem.getEmail();
      final String password = lorem.getWords(1, 8);

      final Personals personal = new Personals(firstName, surname, email, password);
      final int memNo = this.model.addMember(personal, currentDate);
      final int points = this.random.nextInt(80000);
      this.model.registerPoints(memNo, points);
    }

    this.model.checkMembers(currentDate);
  }

  private void refreshView() {
    this.data.setAll(this.model.getMembers());
    this.updateGlobalPercentage();

    this.table.requestFocus();
    this.table.getSelectionModel().select(0);
    this.table.getFocusModel().focus(0);
    this.selectedBonusMember.set(this.table.getSelectionModel().selectedItemProperty().getValue());
  }

  private void updateGlobalPercentage() {
    final List<BonusMember> members = this.model.getMembers();
    final int numOfMembers = members.size();
    int numOfGoldMembers = 0;
    int numOfSilverMembers = 0;
    int numOfBasicMembers = 0;

    for (BonusMember member : members) {
      if (member instanceof GoldMember) {
        ++numOfGoldMembers;
      } else if (member instanceof SilverMember) {
        ++numOfSilverMembers;
      } else {
        ++numOfBasicMembers;
      }
    }

    this.numberOfMem.setValue(numOfMembers);
    this.numberOfSilver.setValue(numOfSilverMembers);
    this.numberOfGold.setValue(numOfGoldMembers);
    this.numberOfBasic.setValue(numOfBasicMembers);

    this.memberString.setValue("" + this.numberOfMem.get());

    final String percentageFormat = "%02.2f";
    final String goldMemPercent = String.format(percentageFormat,
        ((double) numOfGoldMembers / (double) numOfMembers) * 100.0d);
    final String silverMemPercent = String.format(percentageFormat,
        ((double) numOfSilverMembers / (double) numOfMembers) * 100.0d);
    final String basicMemPercent = String.format(percentageFormat,
        ((double) numOfBasicMembers / (double) numOfMembers) * 100.0d);

    this.goldPercentString.setValue(
        "" + this.numberOfGold.get() + " / "
            + this.numberOfMem.get() + "\t(" + goldMemPercent + "%)");
    this.silverPercentString.setValue(
        "" + this.numberOfSilver.get() + " / "
            + this.numberOfMem.get() + "\t(" + silverMemPercent + "%)");
    this.basicPercentString.setValue(
        "" + this.numberOfBasic.get() + " / "
            + this.numberOfMem.get() + "\t(" + basicMemPercent + "%)");

    this.setPieData();
  }

  private void setPieData() {
    pieChartData.setAll(
        new PieChart.Data("Gold members", this.numberOfGold.get()),
        new PieChart.Data("Silver members", this.numberOfSilver.get()),
        new PieChart.Data("Basic members", this.numberOfBasic.get())
    );
  }

  private void createView() {
    this.createTableView();
    this.selectedMemberInfoView.getChildren().setAll(this.createSelectedMemberInfoView());
    final Node toolBar = this.createToolBar();
    final Node globalMemberInfoView = this.createGlobalMemberInfoView();

    this.root.setRight(this.selectedMemberInfoView);
    this.root.setCenter(this.table);
    this.root.setTop(toolBar);
    this.root.setLeft(globalMemberInfoView);
    this.root.setPadding(new Insets(10));
  }

  private void createTableView() {
    TableColumn<BonusMember, String> memberIdCol = new TableColumn<>("Member id");
    memberIdCol.setCellValueFactory(new PropertyValueFactory<BonusMember, String>("MemberNo"));

    TableColumn<BonusMember, String> surNameCol = new TableColumn<>("Surname");
    surNameCol.setCellValueFactory(param -> {
          final String surName = param.getValue().getPersonals().getSurname();
          return new SimpleStringProperty(surName);
        }
    );

    TableColumn<BonusMember, String> emailCol = new TableColumn("Email");
    emailCol.setCellValueFactory(param -> {
          final String emailAddress = param.getValue().getPersonals().getEMailAddress();
          return new SimpleStringProperty(emailAddress);
        }
    );

    TableColumn<BonusMember, String> bonusStateCol = new TableColumn("Bonus state");
    bonusStateCol.setCellValueFactory(param -> {
      if (param.getValue() instanceof GoldMember) {
        return new SimpleStringProperty("Gold member");
      }
      if (param.getValue() instanceof SilverMember) {
        return new SimpleStringProperty("Silver member");
      }
      return new SimpleStringProperty("Basic member");
    });

    this.table = new TableView<>();
    this.table.setEditable(true);
    this.table.setItems(data);
    this.table.getSelectionModel().selectedItemProperty().addListener((obs, oldSel, newSel) -> {
      this.selectedBonusMember.set(newSel);
      this.selectedMemberInfoView.getChildren().setAll(this.createSelectedMemberInfoView());
    });
    this.table.getColumns().addAll(memberIdCol, surNameCol, emailCol, bonusStateCol);

    this.table.requestFocus();
    this.table.getSelectionModel().select(0);
    this.table.getFocusModel().focus(0);
    this.selectedBonusMember.set(this.table.getSelectionModel().selectedItemProperty().getValue());
  }

  private Node createSelectedMemberInfoView() {
    VBox rightInfoBox = new VBox();
    rightInfoBox.setSpacing(20);

    Label title = new Label("Member details");
    title.setAlignment(Pos.TOP_LEFT);
    title.setStyle("-fx-font-size:18; -fx-font-weight:bold;");

    Label userInfoLabel = new Label("User data");
    userInfoLabel.setStyle("-fx-font-size:15; -fx-font-weight:bold;");
    GridPane personalInfoGrid = new GridPane();
    personalInfoGrid.setHgap(10);
    personalInfoGrid.setVgap(5);

    Label memberInfoLabel = new Label("Member data");
    memberInfoLabel.setStyle("-fx-font-size:15; -fx-font-weight:bold;");
    GridPane memberInfoGrid = new GridPane();
    memberInfoGrid.setHgap(10);
    memberInfoGrid.setVgap(5);

    VBox userMemberSeparator = new VBox();
    userMemberSeparator.setPrefHeight(20);

    rightInfoBox.setAlignment(Pos.TOP_LEFT);
    rightInfoBox.setPrefWidth(310);
    rightInfoBox.getChildren().addAll(
        title,
        userInfoLabel,
        personalInfoGrid,
        userMemberSeparator,
        memberInfoLabel,
        memberInfoGrid);
    rightInfoBox.setPadding(new Insets(15));

    final BonusMember member = this.selectedBonusMember.getValue();

    if (member != null) {
      final Personals personals = member.getPersonals();

      if (personals != null) {
        final TextField firstName = new TextField(personals.getFirstname());
        firstName.setEditable(false);
        final TextField surname = new TextField(personals.getSurname());
        surname.setEditable(false);
        final TextField eMailAddress = new TextField(personals.getEMailAddress());
        eMailAddress.setEditable(false);

        personalInfoGrid.add(new Label("First name: "), 0, 0);
        personalInfoGrid.add(firstName, 1, 0);
        personalInfoGrid.add(new Label("Surname: "), 0, 1);
        personalInfoGrid.add(surname, 1, 1);
        personalInfoGrid.add(new Label("Email: "), 0, 2);
        personalInfoGrid.add(eMailAddress, 1, 2);
      }

      final TextField points = new TextField("" + member.getPoints());
      points.setEditable(false);
      final TextField memberNo = new TextField("" + member.getMemberNo());
      memberNo.setEditable(false);
      final LocalDate enrolledDate = member.getEnrolledDate();
      final TextField enrolledDateText = new TextField(
          "" + enrolledDate.getDayOfMonth() + "/"
              + enrolledDate.getMonthValue() + "/" + enrolledDate.getYear()
      );

      memberInfoGrid.add(new Label("Member id: "), 0, 0);
      memberInfoGrid.add(memberNo, 1, 0);
      memberInfoGrid.add(new Label("Points: "), 0, 1);
      memberInfoGrid.add(points, 1, 1);
      memberInfoGrid.add(new Label("Enrolled date: "), 0, 2);
      memberInfoGrid.add(enrolledDateText, 1, 2);
    }

    return rightInfoBox;
  }

  private Node createToolBar() {
    Button addUserButton = new Button();
    addUserButton.setStyle(MemberGui.BUTTON_LABEL_FONT_STYLE);
    addUserButton.setText("Add member");
    addUserButton.setContentDisplay(ContentDisplay.TOP);

    ImageView addUserImageView = new ImageView(
        new Image(
            this.getClass().getResource(MemberGui.ADD_USER_ICON).toExternalForm()
        )
    );

    addUserImageView.setFitWidth(40);
    addUserImageView.setFitHeight(40);
    addUserButton.setOnMouseClicked(event -> this.controller.addNewMember());
    addUserButton.setGraphic(addUserImageView);

    Button removeUserButton = new Button();
    removeUserButton.setStyle(MemberGui.BUTTON_LABEL_FONT_STYLE);
    removeUserButton.setText("Remove member");
    removeUserButton.setContentDisplay(ContentDisplay.TOP);
    removeUserButton.setOnMouseClicked(event -> {
      if (this.selectedBonusMember != null) {
        final BonusMember selectedMember = this.selectedBonusMember.get();
        if (selectedMember != null) {
          this.controller.removeMember(selectedMember);
        }
      }
    });

    ImageView removeUserImageView = new ImageView(
        new Image(
            this.getClass().getResource(MemberGui.REMOVE_USER_ICON).toExternalForm()
        )
    );

    removeUserImageView.setFitWidth(40);
    removeUserImageView.setFitHeight(40);
    removeUserButton.setGraphic(removeUserImageView);

    Button upgradeMembersButton = new Button();
    upgradeMembersButton.setStyle(MemberGui.BUTTON_LABEL_FONT_STYLE);
    upgradeMembersButton.setText("Upgrade members");
    upgradeMembersButton.setContentDisplay(ContentDisplay.TOP);
    upgradeMembersButton.setOnMouseClicked(event -> {
      this.controller.registerPoints(100);
      this.refreshView();
    });

    ImageView upgradeMembersImageView = new ImageView(
        new Image(
            this.getClass().getResource(MemberGui.UPGRADE_USER_ICON).toExternalForm()
        )
    );

    upgradeMembersImageView.setFitWidth(50);
    upgradeMembersImageView.setFitHeight(40);
    upgradeMembersButton.setGraphic(upgradeMembersImageView);

    ToolBar toolBar = new ToolBar();
    toolBar.getItems().addAll(addUserButton, removeUserButton, upgradeMembersButton);
    return toolBar;
  }

  private Node createGlobalMemberInfoView() {
    this.updateGlobalPercentage();

    VBox globalMemberInfoRoot = new VBox();
    globalMemberInfoRoot.setSpacing(20);
    globalMemberInfoRoot.setAlignment(Pos.TOP_LEFT);
    globalMemberInfoRoot.setPadding(new Insets(10, 20, 10, 20));

    Label title = new Label("Bonus Members Statistics");
    title.setStyle("-fx-font-size:18; -fx-font-weight:bold;");
    globalMemberInfoRoot.getChildren().add(title);

    GridPane membersInfoGrid = new GridPane();
    membersInfoGrid.setVgap(5);
    membersInfoGrid.setHgap(10);

    membersInfoGrid.add(new Label("Registered members: "), 0, 0);
    this.memberString.setValue(this.numberOfMem.asString().get());
    final Label numberOfMembersLabel = new Label();
    numberOfMembersLabel.textProperty().bind(this.memberString);
    membersInfoGrid.add(numberOfMembersLabel, 1, 0);

    membersInfoGrid.add(new Label("Gold members: "), 0, 1);
    final Label numberOfGoldMembersLabel = new Label();
    numberOfGoldMembersLabel.textProperty().bind(this.goldPercentString);
    membersInfoGrid.add(numberOfGoldMembersLabel, 1, 1);

    membersInfoGrid.add(new Label("Silver members: "), 0, 2);
    final Label numberOfSilverMembersLabel = new Label();
    numberOfSilverMembersLabel.textProperty().bind(this.silverPercentString);
    membersInfoGrid.add(numberOfSilverMembersLabel, 1, 2);

    membersInfoGrid.add(new Label("Basic members: "), 0, 3);
    final Label numberOfBasicMembersLabel = new Label();
    numberOfBasicMembersLabel.textProperty().bind(this.basicPercentString);
    membersInfoGrid.add(numberOfBasicMembersLabel, 1, 3);

    globalMemberInfoRoot.getChildren().add(membersInfoGrid);

    this.setPieData();

    final PieChart membersPie = new PieChart();
    membersPie.setData(pieChartData);
    membersPie.setPrefSize(450, 450);

    globalMemberInfoRoot.getChildren().add(membersPie);

    return globalMemberInfoRoot;
  }

  @Override
  public void update() {
    this.refreshView();
  }
}