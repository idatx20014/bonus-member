package com.klungerbo.bonusapp.application;

import com.klungerbo.bonusapp.ui.MemberGui;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Contains the entry point of the application and is responsible
 * for selecting the popper type of application to run.
 *
 * @author Tomas Klunberbo Olsen
 * @since 1.1
 */
public final class Application {
  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /**
   * Entry point for the application.
   *
   * @param args command line arguments
   */
  public static void main(final String[] args) {
    try {
      Application.initLogger();
      BonusMemberApp app = new MemberGui();
      app.initialize(args);
      app.execute();
    } catch (Exception e) {
      /* An exception has made it all the way back to start
      which is treated as severe. Give the whole stack to
      the logger for quicker debugging of the issue.
       */
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  private static void initLogger() {
    LogManager.getLogManager().reset();
    LOGGER.setLevel(Level.ALL);

    ConsoleHandler consoleHandler = new ConsoleHandler();
    consoleHandler.setLevel(Level.FINEST);
    LOGGER.addHandler(consoleHandler);

    try {
      FileHandler fileHandler = new FileHandler("./logfile.log");
      fileHandler.setLevel(Level.CONFIG);
      LOGGER.addHandler(fileHandler);
    } catch (IOException e) {
      LOGGER.warning("Could not create the logfile");
    }
  }
}
