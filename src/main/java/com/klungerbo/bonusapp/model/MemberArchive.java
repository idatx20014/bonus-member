package com.klungerbo.bonusapp.model;

import com.klungerbo.bonusapp.model.member.BasicMember;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.model.member.GoldMember;
import com.klungerbo.bonusapp.model.member.SilverMember;
import com.klungerbo.bonusapp.util.Observable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;

/**
 * Manages bonus members.
 *
 * @author Tomas Klungerbo Olsen
 * @since 0.2
 */
public class MemberArchive extends Observable implements Iterable<BonusMember> {
  private static final String ILLEGAL_ARGUMENT_MESSAGE = "One or more arguments are invalid";

  private static final int SILVER_LIMIT = 25000;
  private static final int GOLD_LIMIT = 75000;

  private Random rand;
  private List<BonusMember> members;

  /**
   * Initializes default state for a MemberArchive.
   */
  public MemberArchive() {
    this.members = new ArrayList<>();
    this.rand = new Random();
  }

  /**
   * Get members collected by the archive.
   *
   * @return collection of members.
   */
  public List<BonusMember> getMembers() {
    return this.members;
  }

  /**
   * Remove a member if it exists in the stored collection.
   *
   * @param member the member to remove
   */
  public void removeMember(final BonusMember member) {
    if (member == null) {
      throw new IllegalArgumentException("Bonus member can't be null");
    }

    boolean found = false;
    final Iterator<BonusMember> memberItr = this.members.iterator();

    while (memberItr.hasNext() && !found) {
      final BonusMember currentMember = memberItr.next();
      if (currentMember == member) {
        memberItr.remove();
        this.notifySubscribers();
        found = true;
      }
    }
  }

  /**
   * Checks through all members and upgrades those who are.
   * qualified
   *
   * @param date the current date
   * @throws IllegalArgumentException if the date is invalid
   */
  public void checkMembers(final LocalDate date) {
    if (date == null) {
      throw new IllegalArgumentException("Can't check members with an invalid date");
    }

    for (int i = 0; i < this.members.size(); i++) {
      BonusMember member = this.members.get(i);

      if (member != null) {
        if (this.canUpgradeToGold(member, date)) {
          BonusMember goldMember = this.upgradeToGold(member, date);
          this.members.set(i, goldMember);
          this.notifySubscribers();
        } else if (this.canUpgradeToSilver(member, date)) {
          BonusMember silverMember = this.upgradeToSilver(member, date);
          this.members.set(i, silverMember);
          this.notifySubscribers();
        }
      }
    }
  }

  /**
   * Checks if a member can be upgraded to gold.
   *
   * @param member the member that wants to be upgraded
   * @param date   the current date
   * @return true if the member can be upgraded to gold, false if else
   * @throws IllegalArgumentException if one or more arguments are invalid
   */
  private boolean canUpgradeToGold(final BonusMember member, final LocalDate date) {
    if (member == null || date == null) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    int collectedPoints = member.findQualificationPoints(date);

    boolean validForUpgrade = member instanceof BasicMember || member instanceof SilverMember;
    validForUpgrade = validForUpgrade && collectedPoints > 0;
    validForUpgrade = validForUpgrade && collectedPoints >= GOLD_LIMIT;

    return validForUpgrade;
  }

  /**
   * Upgrade a bonus member to a silver member if allowed.
   *
   * @param member the member to upgrade
   * @param date   the current date
   * @return the member in its current form
   * @throws IllegalArgumentException if member or date is null or if the member
   *                                  does not meet upgrade qualifications
   */
  private BonusMember upgradeToGold(final BonusMember member, final LocalDate date) {
    if (member == null || date == null) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    BonusMember returnMember;

    if (this.canUpgradeToGold(member, date)) {
      returnMember = new GoldMember(
          member.getMemberNo(),
          member.getPersonals(),
          member.getEnrolledDate(),
          member.getPoints()
      );
    } else {
      throw new IllegalArgumentException("The member is not qualified for the upgrade");
    }

    return returnMember;
  }

  /**
   * Checks if a member can be upgraded to silver.
   *
   * @param member the member that wants to be upgraded
   * @param date   the current date
   * @return true if it can be upgraded to silver, false if else
   * @throws IllegalArgumentException if member or date is null
   */
  private boolean canUpgradeToSilver(final BonusMember member, final LocalDate date) {
    if (member == null || date == null) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    int collectedPoints = member.findQualificationPoints(date);

    boolean validForUpgrade = member instanceof BasicMember;
    validForUpgrade = validForUpgrade && collectedPoints >= SILVER_LIMIT;

    return validForUpgrade;
  }

  /**
   * Upgrade a bonus member to a silver member if allowed.
   *
   * @param member the member to upgrade
   * @param date   the current date
   * @return the member in its current form
   * @throws IllegalArgumentException if member or date is null or if the member
   *                                  does not meet upgrade qualifications
   */
  private BonusMember upgradeToSilver(final BonusMember member, final LocalDate date) {
    if (member == null || date == null) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    BonusMember returnMember;

    if (this.canUpgradeToSilver(member, date)) {
      returnMember = new SilverMember(
          member.getMemberNo(),
          member.getPersonals(),
          member.getEnrolledDate(),
          member.getPoints()
      );
    } else {
      throw new IllegalArgumentException("The member is not qualified for the upgrade");
    }

    return returnMember;
  }

  /**
   * Add a bonus member to the archive.
   *
   * @param personals    the personals information connected with the membership
   * @param dateEnrolled the date this member enrolled as a member
   * @return the members identification number
   */
  public int addMember(final Personals personals, final LocalDate dateEnrolled) {
    if (personals == null || dateEnrolled == null) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    int memberNo = this.findAvailableNo();
    BonusMember member = new BasicMember(memberNo, personals, dateEnrolled);
    this.members.add(member);
    this.notifySubscribers();

    return memberNo;
  }

  /**
   * Find an available member number.
   *
   * @return the available member number
   */
  private int findAvailableNo() {
    int memberNo = -1;

    while (memberNo == -1) {
      int num = this.rand.nextInt(Integer.MAX_VALUE);
      if (this.getMember(num) == null) {
        memberNo = num;
      }
    }

    return memberNo;
  }

  /**
   * Finds a member by its identification number.
   *
   * @param memberNo the member identification number
   * @return the member if it was found, null if else
   */
  public BonusMember getMember(final int memberNo) {
    Optional<BonusMember> member = this.members
        .parallelStream()
        .filter(bonusMember -> bonusMember.getMemberNo() == memberNo)
        .findFirst();

    return member.orElse(null);
  }

  /**
   * Register points to a member.
   *
   * @param memberNo the member identification number to add points to
   * @param points   the points to be added
   * @return true if the member exists and it received its points, false if else
   * @throws IllegalArgumentException if one or more arguments are invalid
   */
  public boolean registerPoints(final int memberNo, final int points) {
    if (memberNo < 0 || points <= 0) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    BonusMember member = this.getMember(memberNo);
    if (member != null) {
      member.registerPoints(points);
    }

    return member != null;
  }

  /**
   * Finds the points connected to a specific member.
   *
   * @param memberNo the member identification number to look for
   * @param password the password of the member for verification
   * @return the amount of points the member has
   * @throws IllegalArgumentException if one or more arguments are invalid
   */
  public int findPoints(final int memberNo, final String password) {
    if (memberNo < 0 || password == null || password.isBlank()) {
      throw new IllegalArgumentException(MemberArchive.ILLEGAL_ARGUMENT_MESSAGE);
    }

    int returnValue = -1;

    BonusMember member = this.getMember(memberNo);
    if (member != null && member.okPassword(password)) {
      returnValue = member.getPoints();
    }

    return returnValue;
  }

  @Override
  public Iterator<BonusMember> iterator() {
    return this.members.iterator();
  }

  @Override
  public void forEach(Consumer<? super BonusMember> action) {
    this.members.forEach(action);
  }
}
