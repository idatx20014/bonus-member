package com.klungerbo.bonusapp.application;

/**
 * Interface for a bonus member application which is used to
 * launch either GUI or CLI.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public interface BonusMemberApp {
  void initialize(final String[] args);

  void execute();
}
