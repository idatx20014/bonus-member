package com.klungerbo.bonusapp.controller;

import com.klungerbo.bonusapp.model.MemberArchive;
import com.klungerbo.bonusapp.model.Personals;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.ui.AddBonusMemberDialog;
import java.time.LocalDate;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * The glue between view and model layer for better decoupling.
 * Does logic on models based on input from view layer.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.3
 */
public class MemberController {
  private static final String CSS_DARK_THEME_PATH = "/css/style.css";
  private MemberArchive model;

  /**
   * Initializes the controller with proper model data.
   *
   * @param model the model to do logic on.
   * @throws IllegalArgumentException if arguments are invalid
   */
  public MemberController(final MemberArchive model) {
    if (model == null) {
      throw new IllegalArgumentException("One or more arguments are invalid");
    }

    this.model = model;
  }

  /**
   * Gives points to all bonus members, should only be called if
   * you are feeling extra generous.
   * <p>
   * All members are checked for and upgraded if criteria are met.
   * </p>
   *
   * @param points the points to add to all members
   * @throws IllegalArgumentException if points are not a positive amount
   */
  public void registerPoints(final int points) {
    if (points <= 0) {
      throw new IllegalArgumentException("Can only add positive amount of points");
    }

    final Alert givePointsAlert = new Alert(Alert.AlertType.CONFIRMATION);
    givePointsAlert.getDialogPane()
        .getStylesheets()
        .add(this.getClass().getResource(CSS_DARK_THEME_PATH).toExternalForm());
    givePointsAlert.setTitle("Upgrade members");
    givePointsAlert.setHeaderText("Are you feeling extra generous today?");
    givePointsAlert.setContentText("Give " + points + " points to all members and upgrade those " +
        "who are qualified");

    givePointsAlert.showAndWait()
        .filter(response -> response == ButtonType.OK)
        .ifPresent(response -> {
              for (BonusMember member : this.model) {
                member.registerPoints(points);
              }
              this.model.checkMembers(LocalDate.now());
            }
        );
  }

  /**
   * Removes a member.
   *
   * @param member the member to remove
   * @throws IllegalArgumentException if the member to remove is not valid
   */
  public void removeMember(final BonusMember member) {
    if (member == null) {
      throw new IllegalArgumentException("Cant remove a null member");
    }

    final Alert removeMemberAlert = new Alert(Alert.AlertType.CONFIRMATION);
    removeMemberAlert.setTitle("Remove member");
    removeMemberAlert.setHeaderText("Are you sure you want to permanently remove this member?");

    final Personals personals = member.getPersonals();
    final String memberInfoMessage = String.format("""
            The member that will be removed is:

            First name: %s
            Last name: %s
            Email address: %s
            """,
        personals.getFirstname(),
        personals.getSurname(),
        personals.getEMailAddress()
    );

    removeMemberAlert.setContentText(memberInfoMessage);
    removeMemberAlert.getDialogPane()
        .getStylesheets()
        .add(this.getClass().getResource(CSS_DARK_THEME_PATH).toExternalForm());

    removeMemberAlert.showAndWait()
        .filter(response -> response == ButtonType.OK)
        .ifPresent(response -> this.model.removeMember(member));
  }

  /**
   * Adds a member by creating a new dialogue for users
   * to input data associated with the new member.
   */
  public void addNewMember() {
    AddBonusMemberDialog dialog = new AddBonusMemberDialog();
    dialog.getDialogPane()
        .getStylesheets()
        .add(this.getClass().getResource(CSS_DARK_THEME_PATH).toExternalForm());
    Optional<Personals> optPersonal = dialog.showAndWait();
    optPersonal.ifPresent(user -> this.model.addMember(user, LocalDate.now()));
  }
}
