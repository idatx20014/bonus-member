package com.klungerbo.bonusapp.ui;

import com.klungerbo.bonusapp.model.MemberArchive;
import com.klungerbo.bonusapp.model.Personals;
import com.klungerbo.bonusapp.model.member.BasicMember;
import com.klungerbo.bonusapp.model.member.BonusMember;
import com.klungerbo.bonusapp.model.member.GoldMember;
import com.klungerbo.bonusapp.model.member.SilverMember;
import java.time.LocalDate;
import java.util.Scanner;

/**
 * A Command line interface for managing bonus members.
 *
 * @author Tomas Klungerbo Olsen
 * @since 1.1
 */
public final class MemberCli {
  /**
   * Value for menu entry to add a bonus member.
   **/
  private static final int ADD_BONUS_MEMBER = 1;
  /**
   * Value for menu entry to list all members.
   **/
  private static final int LIST_ALL_MEMBERS = 2;
  /**
   * Value for menu entry to upgrade qualified members.
   **/
  private static final int UPGRADE_QUALIFIED_MEMBERS = 3;
  /**
   * Value for menu entry to register points for a member.
   **/
  private static final int REGISTER_BONUS_POINTS = 4;
  /**
   * Value for menu entry to list all members sorted by points.
   **/
  private static final int LIST_ALL_MEMBERS_SORTED_BY_POINTS_ASCENDING = 5;
  /**
   * Value for menu entry to exit the application.
   **/
  private static final int QUIT = 6;

  /**
   * holds the running status of the application.
   **/
  private boolean running;

  /**
   * A reference to the archive responsible for the member logic.
   **/
  private MemberArchive archive;

  /**
   * Default constructor setting up default values.
   */
  public MemberCli() {
    this.running = true;
    this.archive = new MemberArchive();
  }

  /**
   * Initializes the instance. Should be called on post creation
   */
  public void init(String[] args) {
    this.generateTestPersons();
  }

  private void generateTestPersons() {
    final Personals donald = new Personals(
        "Donald",
        "Peters",
        "dnld.mrsson@dot.com",
        "morrison123");
    final Personals kole = new Personals(
        "Kole",
        "Francis",
        "kole.francis@dot.com",
        "kole1234");
    final Personals brody = new Personals(
        "Brody",
        "Hess",
        "hess.brody@dot.com",
        "brodyHess");
    final Personals elsa = new Personals(
        "Else",
        "York",
        "elsa.elsa@dot.com",
        "elsa");
    final Personals madden = new Personals(
        "Madden",
        "White",
        "white.madden@dot.com",
        "white");

    final LocalDate currentDate = LocalDate.now();
    this.archive.addMember(donald, currentDate);
    this.archive.addMember(kole, currentDate);
    this.archive.addMember(brody, currentDate);
    this.archive.addMember(elsa, currentDate);
    this.archive.addMember(madden, currentDate);
  }

  private void showMenu() {
    System.out.println("Bonus member application");
    System.out.println("1. Add bonus member");
    System.out.println("2. List all members");
    System.out.println("3. Upgrade qualified members");
    System.out.println("4. Register bonus points");
    System.out.println("5. List all members sorted by points ascending");
    System.out.println("6. Quit");
    System.out.println();
  }

  private int getMenuChoice() {
    final int minValue = 1;
    final int maxValue = 6;
    return this.getValidIntInput(minValue, maxValue, this::showMenu);
  }

  /**
   * Executes the user interface.
   */
  public void execute() {
    while (this.running) {
      int choice = this.getMenuChoice();

      switch (choice) {
        case ADD_BONUS_MEMBER -> this.addBonusMember();
        case LIST_ALL_MEMBERS -> this.listAllMembers();
        case UPGRADE_QUALIFIED_MEMBERS -> this.upgradeQualifiedMembers();
        case REGISTER_BONUS_POINTS -> this.registerBonusPoints();
        case LIST_ALL_MEMBERS_SORTED_BY_POINTS_ASCENDING -> this.displayAllMembersSortedByPoints();
        case QUIT -> this.quitApp();
        default -> System.out.println("Not a valid command");
      }
    }
  }

  private Personals createPersonalFromUserInput() {
    Personals person = null;
    final byte maxAttempts = 3;
    byte attempts = 0;
    boolean success = false;

    do {
      final String firstName = this.getStringInput(
          () -> System.out.println("First name:"));
      final String lastName = this.getStringInput(
          () -> System.out.println("Last name:"));
      final String emailAddress = this.getStringInput(
          () -> System.out.println("Email Address:"));
      final String password = this.getStringInput(
          () -> System.out.println("Password: "));

      try {
        person = new Personals(
            firstName,
            lastName,
            emailAddress,
            password);

        success = true;
      } catch (IllegalArgumentException e) {
        ++attempts;
        System.out.println(e.getMessage());
      }
    } while (!success && attempts < maxAttempts);

    return person;
  }

  private LocalDate dateFromUserInput() {
    final int year = this.getValidIntInput(
        1900,
        LocalDate.now().getYear(),
        () -> System.out.println("Year:"));
    final int month = this.getValidIntInput(
        1,
        12,
        () -> System.out.println("Month:"));
    final int dayOfMonth = this.getValidIntInput(
        1,
        31,
        () -> System.out.println("Day of month:"));

    return LocalDate.of(year, month, dayOfMonth);
  }

  private int getValidIntInput(
      final int min,
      final int max,
      final Runnable message) {
    message.run();
    System.out.print("> ");

    Scanner scanner = new Scanner(System.in);
    int chosenInt;

    do {
      while (!scanner.hasNextInt()) {
        scanner.nextLine();
        System.out.println("Input needs to be a number");
        System.out.print("> ");
      }
      chosenInt = scanner.nextInt();

      if ((chosenInt < min) || (chosenInt > max)) {
        System.out.println(
            "Number needs to be between ["
                + min
                + ","
                + max
                + "]");
        System.out.print("> ");
      }

    } while ((chosenInt < min) || (chosenInt > max));

    return chosenInt;
  }

  private LocalDate createDateFromUserInput() {
    final int dateSelection = this.getValidIntInput(
        1,
        2,
        () -> {
          System.out.println();
          System.out.println(
              "1. Register member using current date");
          System.out.println(
              "2. Register member by manually setting date");
        });

    LocalDate date = null;

    switch (dateSelection) {
      case 1 -> date = LocalDate.now();
      case 2 -> date = this.dateFromUserInput();
      default -> System.out.println("A non valid option was chosen");
    }

    return date;
  }

  private void addBonusMember() {
    Personals person = this.createPersonalFromUserInput();
    LocalDate date = this.createDateFromUserInput();

    if (person != null && date != null) {
      this.archive.addMember(person, date);
    } else {
      System.out.println("Could not create the bonus member");
    }
  }

  private void listAllMembers() {
    this.archive.forEach(this::displayMember);
    System.out.println();
  }

  private void displayAllMembersSortedByPoints() {
    this.archive.getMembers().stream()
        .sorted()
        .forEach(this::displayMember);
  }

  private void displayBasicMember() {
    System.out.println("Member status: Basic");
  }

  private void displaySilverMember() {
    System.out.println("Member status: Silver");
  }

  private void displayGoldMember() {
    System.out.println("Member status: Gold");
  }

  private void displayMemberSpecificInfo(final BonusMember member) {
    System.out.println("Member number: " + member.getMemberNo());
    System.out.println("Collected points: " + member.getPoints());
    System.out.println(
        "Points qualified for upgrade: "
            + member.findQualificationPoints(LocalDate.now()));
    System.out.println();
  }

  private void displayPersonalSpecificInfo(final BonusMember member) {
    final Personals person = member.getPersonals();

    if (person != null) {
      System.out.println("[Account information]");
      System.out.println("First name: " + person.getFirstname());
      System.out.println("Last name: " + person.getSurname());
      System.out.println("Email Address: " + person.getEMailAddress());
    }
  }

  private void displayBonusMemberSpecificInfo(final BonusMember member) {
    if (member instanceof BasicMember) {
      this.displayBasicMember();
    } else if (member instanceof SilverMember) {
      this.displaySilverMember();
    } else if (member instanceof GoldMember) {
      this.displayGoldMember();
    }
  }

  private void displayMember(final BonusMember member) {
    if (member != null) {
      System.out.println(
          "**************************************************");

      this.displayBonusMemberSpecificInfo(member);
      this.displayMemberSpecificInfo(member);
      this.displayPersonalSpecificInfo(member);

      System.out.println(
          "--------------------------------------------------");
      System.out.println();
    }
  }

  private void upgradeQualifiedMembers() {
    this.archive.checkMembers(LocalDate.now());
  }

  private void registerBonusPoints() {
    final int memberNo = this.getIntInput(
        () -> System.out.println("Member number: "));
    final BonusMember member = this.archive.getMember(memberNo);

    if (member == null) {
      System.out.println("There are no members with that number");
    } else {
      int points;
      do {
        points = this.getIntInput(() -> System.out.println("Points: "));
      } while (points < 0);

      if (this.archive.registerPoints(memberNo, points)) {
        System.out.println(
            "Successfully added "
                + points
                + " to member with number "
                + memberNo);
      }
    }

    System.out.println();
  }

  /**
   * Everything to be done before app is closing.
   */
  private void quitApp() {
    this.running = false;
  }

  private int getIntInput(final Runnable message) {
    message.run();
    System.out.print("> ");
    Scanner scanner = new Scanner(System.in);

    while (!scanner.hasNextInt()) {
      scanner.nextLine();
      System.out.println("Input needs to be a number");
      System.out.print("> ");
    }

    return scanner.nextInt();
  }

  private String getStringInput(final Runnable message) {
    message.run();
    System.out.print("> ");
    Scanner scanner = new Scanner(System.in);
    return scanner.nextLine();
  }
}
